import React, { useState } from "react";
import Link from "next/link";

import "./styles/SideNavigation.scss";
import MenuTab from "../../assets/icons/MenuTab";
import WalletIcon from "../../assets/icons/WalletIcon";
import WagerIcon from "../../assets/icons/WagerIcon";
import EscrowIcon from "../../assets/icons/EscrowIcon";
import MarketIcon from "../../assets/icons/MarketIcon";
import SubscriptionsIcon from "../../assets/icons/SubscriptionsIcon";
import GlobalTrade from "../../assets/icons/GlobalTrade";
import scrollToRef from "../../functions/scrollToRef";

export default function SideNavigation({anchorRefs}) {
  const [isOpen, setIsOpen] = useState(false);
  const toggleOpenState = () => setIsOpen(prev => !prev);
  return (
    <div className={`sidenav ${isOpen ? "open" : ""}`}>
      <span
        className={`menu-tab ${isOpen ? "open" : ""}`}
        onClick={toggleOpenState}
      >
        <MenuTab />
      </span>
      <div className="sidenav-links">
        <Link href="#">
          <a onClick={() => scrollToRef(anchorRefs.walletRef)}>
            <span className="icon">
              <WalletIcon />
            </span>
            <span className="label">Wallet</span>
          </a>
        </Link>
        <Link href="#">
          <a onClick={() => scrollToRef(anchorRefs.wagerRef)}>
            <span className="icon">
              <WagerIcon />
            </span>
            <span className="label">Wagering</span>
          </a>
        </Link>
        <Link href="#">
          <a onClick={() => scrollToRef(anchorRefs.escrowRef)}>
            <span className="icon">
              <EscrowIcon />
            </span>
            <span className="label">Escrow</span>
          </a>
        </Link>
        <Link href="#">
          <a onClick={() => scrollToRef(anchorRefs.marketRef)}>
            <span className="icon">
              <GlobalTrade />
            </span>
            <span className="label">Market</span>
          </a>
        </Link>
        <Link href="#">
          <a onClick={() => scrollToRef(anchorRefs.subscriptionsRef)}>
            <span className="icon">
              <SubscriptionsIcon />
            </span>
            <span className="label">Subscriptions</span>
          </a>
        </Link>
      </div>
    </div>
  );
}
