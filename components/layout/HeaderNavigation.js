import React from "react";
import BrandLogo from "../../assets/icons/BrandLogo";
import Link from "next/link";
import "./styles/HeaderNavigation.scss";
import "../../assets/styles/global.scss";
import SideNavigation from "./SideNavigation";
import scrollToAnchorID from "../../functions/scrollToRef";

export default function HeaderNavigation({anchorRefs}) {
  return (
    <header className="header-navigation">
      <nav className="header-navigation-navbar">
        <div className="brand-wrapper">
          <BrandLogo />
        </div>
        <div className="links">
          <Link href="#">
            <a onClick={() => scrollToAnchorID(anchorRefs.walletRef)}>Wallet</a>
          </Link>
          <Link href="#">
            <a onClick={() => scrollToAnchorID(anchorRefs.wagerRef)}>Wagering</a>
          </Link>
          <Link href="#">
            <a onClick={() => scrollToAnchorID(anchorRefs.escrowRef)}>Escrow</a>
          </Link>
          <Link href="#">
            <a onClick={() => scrollToAnchorID(anchorRefs.marketRef)}>Market</a>
          </Link>
          <Link href="#">
            <a onClick={() => scrollToAnchorID(anchorRefs.subscriptionsRef)}>Subscriptions</a>
          </Link>
        </div>
        <SideNavigation anchorRefs={anchorRefs}/>
      </nav>
    </header>
  );
}
