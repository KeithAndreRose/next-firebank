import React, { useRef } from "react";
import Head from "next/head";
import "./styles/index.scss";
import SiteFooter from "../components/SiteFooter";
import HeaderNavigation from "../components/layout/HeaderNavigation";
import BrandLogo from "../assets/icons/BrandLogo";
import SubscriptionsIcon from "../assets/icons/SubscriptionsIcon";
import EscrowIcon from "../assets/icons/EscrowIcon";
import MarketIcon from "../assets/icons/MarketIcon";
import WagerIcon from "../assets/icons/WagerIcon";
import WalletIcon from "../assets/icons/WalletIcon";
import MobileBanking from "../assets/icons/MobileBanking";
import LedgerIcon from "../assets/icons/LedgerIcon";
import GlobalTrade from "../assets/icons/GlobalTrade";
import scrollToRef from "../functions/scrollToRef";

const Home = () => {
  const anchorRefs = {
  walletRef: useRef(null),
  wagerRef : useRef(null),
  escrowRef : useRef(null),
  marketRef : useRef(null),
  subscriptionsRef : useRef(null),
  waitlistRef : useRef(null),
  }

  return (
    <div className="HomePage">
      <Head>
        <title>Firebank 🔥</title>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="/apple-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="/apple-icon-60x60.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="/apple-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="/apple-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href="/apple-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href="/apple-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="/apple-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="/apple-icon-152x152.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-icon-180x180.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="/android-icon-192x192.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="96x96"
          href="/favicon-96x96.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="manifest" href="/manifest.json" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
        <meta name="theme-color" content="#ffffff"></meta>
      </Head>
      <HeaderNavigation anchorRefs={anchorRefs} />
      <div className="page-content">
        <div className="hero">
          <div className="gradient g1"></div>
          <h3 className="brand-name">Firebank 🔥</h3>
          <div className="hero-content">
            <div className="text-container">
              <p className="brand-summary">
                The all-in-one international blockchain market and service
                provider.
              </p>
              <a className="join-list" href="#" onClick={() => scrollToRef(anchorRefs.waitlistRef)}>Join waitlist!</a>
            </div>
            <div className="visual">
              <div className="content">
                <BrandLogo />
              </div>
            </div>
          </div>
        </div>
        <div className="info-columns">
          <div className="column1">
            <div className="column-item">
              <div className="item-content">
                <h3>A Global Platform</h3>
                <p>
                  Buy and connect on a international level. Connectivity with
                  the world with a international currency and international
                  ledger on the ethereum blockchain.
                </p>
                <span className="visual">
                  <span className="visual-content">
                    <MarketIcon />
                  </span>
                </span>
              </div>
            </div>
            <div className="column-item">
              <div className="item-content">
                <h3>Simply Transparency</h3>
                <p>
                  Easily verify the transactions and monetary handling of your
                  your assets. Enjoy digital and physical copies of EVERYTHING
                  free for parties involed.
                </p>
                <span className="visual">
                  <span className="visual-content">
                    <LedgerIcon />
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="column2">
            <div className="column-item">
              <div className="item-content">
                <h3>Connecting is Easy and Mobile!</h3>
                <p>
                  The world your your oyster and it's user friendly! Easily
                  connect and contribute to the development of the modern world
                  though any device.
                </p>
                <span className="visual">
                  <span className="visual-content">
                    <MobileBanking />
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="info-rows">
        <div className="gradient g2"></div>

          <div className="info-row" ref={anchorRefs.walletRef} id="wallet">
            <h3>Wallet</h3>
            <p>
              Send and receive your crypto currency with your wallet. Manager
              your own private keys. We only track you if you ask.
            </p>
            <div className="visual">
              <div className="content">
                <WalletIcon />
              </div>
            </div>
          </div>
          <div className="info-row" ref={anchorRefs.wagerRef} id="wager">
            <h3>Wagering</h3>
            <p>
              Place your bets, wagers, gamblings through us. See live
              transactions and placed wagers on our system. Only 2% goes to the
              house (Firebank)
            </p>
            <div className="visual" >
              <div className="content">
                <WagerIcon />
              </div>
            </div>
          </div>
          <div className="info-row" ref={anchorRefs.escrowRef} id="escrow"> 
            <h3>Escrow</h3>
            <p>
              Save your time and security with automated escrow. Ensure all
              conditions are met selling your assets. Track, review, and
              document your transactions with the power of smart contracts.
              Digital and Physical documentaion provided by Firebank to all
              parties.
            </p>
            <div className="visual">
              <div className="content">
                <EscrowIcon />
              </div>
            </div>
          </div>
          <div className="info-row" ref={anchorRefs.marketRef} id="market">
            <h3>Global Market</h3>
            <p>
              Buy direct from manufacturing and traceable/verifiable vendors
              with though our Firebank Bazaar. Buy, sell, and contribute to the
              global economic market.
            </p>
            <div className="visual">
              <div className="content">
                <GlobalTrade />
              </div>
            </div>
          </div>
          <div className="info-row" ref={anchorRefs.subscriptionsRef} id="subscriptions">
              <div className="gradient g3"></div>
            <h3>Subscription Allowance</h3>
            <p>
              Pay your employees, service provider, or subscriptions with a
              routine and scheduled payment system. Assign, control, and share
              access to a custodial wallet provided by Firebank.
            </p>
            <div className="visual">
              <div className="content subscription">

                <SubscriptionsIcon />
              </div>
            </div>
          </div>
        </div>
        <div className="waitlist-container" ref={anchorRefs.waitlistRef} id="waitlist">
          <div className="join-waitlist">Join Waitlist</div>
        </div>
      </div>
      <SiteFooter />
    </div>
  );
};
export default Home;
