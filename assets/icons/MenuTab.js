import React from "react";

export default function MenuTab() {
  return (
    <svg height="16px" width="16px" viewBox="0 0 64 64">
      <path d="M52,18.66H12a2,2,0,0,1,0-4H52a2,2,0,0,1,0,4Z"></path>
      <path d="M52,34H12a2,2,0,0,1,0-4H52a2,2,0,1,1,0,4Z"></path>
      <path d="M52,49.34H12a2,2,0,0,1,0-4H52a2,2,0,0,1,0,4Z"></path>
    </svg>
  );
}
