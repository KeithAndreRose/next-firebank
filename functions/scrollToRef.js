export default function scrollToRef(ref) {
    if(!ref || !ref.current) return 
    const refOffsetTop = ref.current.offsetTop
    const refHeight = ref.current.clientHeight
    const targetY = (refOffsetTop - refHeight)
    console.log({refOffsetTop, refHeight, targetY})

    window.scrollTo(0, targetY) 
    return
}
